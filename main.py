#!/usr/bin/python

import sys
import argparse
import logging
from Transpiler import Transpiler


def main(argv):
	logging.basicConfig(format = '%(asctime)s %(levelname)-8s - %(message)s', level = 'DEBUG')
	parser = argparse.ArgumentParser(description='Upgrade SuiteScript 1.0 to 2.0 (as best as I can)')
	parser.add_argument('files', metavar='file', nargs='+', help='the file(s) to be transpiled')
	args = parser.parse_args()

	transpiler = Transpiler()

	for file in args.files:
		inputFile = file

		transpiler.process(inputFile)


if __name__ == "__main__":
	main(sys.argv[1:])
