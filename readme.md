# SuiteScript Transpiler

A program to help upgrade your SuiteScript 1.0 scripts to 2.x.

Install dependencies with 

```python
python -m pip install jsbeautifier
```

## Run

```python
python main.py <filename(s)>
```

This will replace common functions in `<filename>` and output the new code to `<filename>_2.0.js`

It's okay to pass `<directory>/**/*.js` if your prompt supports it, but there is no internal globbing.

### TODO

- Intelligently choose the module type on behalf of the user, by analysing the function names and making a guess.
- Support fpr more than just regex matching, functions to run on the matches? 
- Support for variables where 'text' has been provided in the past
- SUpport for functions(){} passed as parameters
- Support for more modules (url, render, search)